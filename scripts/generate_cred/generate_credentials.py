from AirTagCrypto import AirTagCrypto
import base64
import random
import base64
import hashlib

print("Enter name of bike (default: random): ")
name = input()

if not name:
    name = str(random.randint(0, 9999)).zfill(4)
print(f"Bike {name}")
print()

crypto = AirTagCrypto(private_key="X8dFKWNVakhrcIzsbZDkXQ9CMDKvHE6M0Kslbg==")

print("Copy into cykel")
print(f"key_id = \"{base64.b64encode(hashlib.sha256(base64.b64decode(crypto.get_advertisement_key())).digest()).decode('utf-8')}\"")
print(f"advertisement_key = \"{crypto.get_advertisement_key()}\"")
print(f"private_key = \"{base64.b64encode(crypto._private_key).decode()}\"")
print(f"???? = \"{crypto.get_public_key()}\"")
