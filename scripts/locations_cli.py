"""Script to print locations from a given tracker.toml file."""

import argparse
from openhaybike.types import BikeTracker
from openhaybike.locations import get_locations_of_trackers
from openhaybike.config import CONFIG

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--key-id", help="Key ID / hashed advertisement key of tracker",
                        type=str, required=True)
    parser.add_argument("--private-key", help="Private key of tracker",
                        type=str, required=True)
    parser.add_argument("--hours", help="Return results from the last x hours",
                        type=float, default=24.0)
    args = parser.parse_args()
    
    tracker = BikeTracker(
        name="",
        key_id=args.key_id,
        advertisement_key="",
        private_key=args.private_key,
    )
    
    print(get_locations_of_trackers([tracker], CONFIG.icloud_key, args.hours))
