"""Runs the openhaybike server."""

import requests
from openhaybike.types import BikeTracker
from openhaybike.locations import get_locations_of_trackers, LocationReport
from openhaybike.config import CONFIG
from typing import List
import toml
import json
from sys import stderr
from typing import Dict, Set
import time
from datetime import datetime

AUTH_HEADER = {"Authorization": f"Api-Key {CONFIG.cykel_api_key}"}


def request_trackers() -> List[BikeTracker]:
    tracker_json = json.loads(
        requests.get(CONFIG.cykel_endpoint + "/tracker", headers=AUTH_HEADER, timeout=7).text
    )

    trackers = []
    for tracker in tracker_json:
        name = tracker["device_id"]
        if tracker["api_key"] is None:
            continue
        api_key_toml = tracker["api_key"].replace('" ', '"\n', 3)
        api_keys = toml.loads(api_key_toml)
        tracker_object = BikeTracker(name=name, **api_keys)
        trackers.append(tracker_object)
    return trackers


def update_all_trackers(last_sent_updates: Dict[str, Set[LocationReport]], delta: float):
    print(f"Updating in the last {delta} hours")
    trackers = request_trackers()
    locations = get_locations_of_trackers(trackers, CONFIG.icloud_key, delta)
    print(f"Downloaded {sum(len(x) for _, x in locations.items())} locations")
    for key, locations in locations.items():
        if not key in last_sent_updates:
            # only send newest location
            last_sent_updates[key] = set(locations[1:])
        for location in set(locations) - last_sent_updates[key]:
            data = location.serialize()
            data["device_id"] = key
            print(time.ctime(), json.dumps(data))
            print(time.ctime(),
                requests.post(
                    CONFIG.cykel_endpoint + "/bike/updatelocation",
                    headers=AUTH_HEADER,
                    json=data,
                    timeout=10,
                ).text
            )
        last_sent_updates[key] |= set(locations)


def run_update_with_freq(freq: float, big_update_every: int):
    last_sent_updates = {}
    big_update_counter = 0
    while True:
        for key in last_sent_updates:
            last_sent_updates[key] = {
                x
                for x in last_sent_updates[key]
                if (datetime.now() - x.timestamp).seconds < 4 * 60 * 60
            }
        update_all_trackers(last_sent_updates, 0.5 if big_update_counter == 0 else 0.05)
        time.sleep(freq)
        big_update_counter += 1
        big_update_counter %= big_update_every

if __name__ == "__main__":
    run_update_with_freq(30, 20)

