"""Downloads locations from the Apple API."""

import os
import glob
from datetime import datetime
import time
import base64
import hashlib
import hmac
import struct
from cryptography.hazmat.primitives.ciphers import algorithms, modes
from cryptography.hazmat.primitives.asymmetric import ec
import objc
from openhaybike.utils import unpad, decrypt, bytes_to_int, sha256
from openhaybike.types import LocationReport, BikeTracker
from typing import Tuple, List, Dict
import requests
from cryptography.hazmat.backends import default_backend
import json

# This import works on MacOS
from Foundation import NSBundle, NSClassFromString, NSData, NSPropertyListSerialization

ENC_DATA_LENGTH = 10
CRYPTO_TAG_LENGTH = 16


def decode_tag(data: bytes, timestamp: datetime) -> LocationReport:
    latitude = struct.unpack(">i", data[0:4])[0] / 10000000.0
    longitude = struct.unpack(">i", data[4:8])[0] / 10000000.0
    confidence = bytes_to_int(data[8:9])
    status = bytes_to_int(data[9:10])
    return LocationReport(
        latitude=latitude, longitude=longitude, confidence=confidence, status=status, timestamp=timestamp
    )


def getAppleDSIDandSearchPartyToken(iCloudKey: str) -> Tuple[bytes, bytes]:
    # copied from https://github.com/Hsn723/MMeTokenDecrypt
    decryption_key = hmac.new(
        b"t9s\"lx^awe.580Gj%'ld+0LG<#9xa?>vb)-fkwb92[}",
        base64.b64decode(iCloudKey),
        digestmod=hashlib.md5,
    ).digest()  # ]
    mmeTokenFile = glob.glob(
        "%s/Library/Application Support/iCloud/Accounts/[0-9]*"
        % os.path.expanduser("~")
    )[0]
    decryptedBinary = unpad(
        decrypt(
            open(mmeTokenFile, "rb").read(),
            algorithms.AES(decryption_key),
            modes.CBC(b"\00" * 16),
        ),
        algorithms.AES.block_size,
    )
    binToPlist = NSData.dataWithBytes_length_(decryptedBinary, len(decryptedBinary))
    tokenPlist = NSPropertyListSerialization.propertyListWithData_options_format_error_(
        binToPlist, 0, None, None
    )[0]
    return (
        tokenPlist["appleAccountInfo"]["dsPrsID"],
        tokenPlist["tokens"]["searchPartyToken"],
    )


def getOTPHeaders() -> Tuple[bytes, bytes]:
    AOSKitBundle = NSBundle.bundleWithPath_(
        "/System/Library/PrivateFrameworks/AOSKit.framework"
    )
    objc.loadBundleFunctions(
        AOSKitBundle, globals(), [("retrieveOTPHeadersForDSID", b"")]
    )
    util = NSClassFromString("AOSUtilities")
    anisette = (
        str(util.retrieveOTPHeadersForDSID_("-2"))
        .replace('"', " ")
        .replace(";", " ")
        .split()
    )
    return anisette[6], anisette[3]


def getCurrentTimes() -> Tuple[str, str, int]:
    clientTime = datetime.utcnow().replace(microsecond=0).isoformat() + "Z"
    clientTimestamp = int(datetime.now().strftime("%s"))
    return clientTime, time.tzname[1], clientTimestamp


def get_locations_of_trackers(
    trackers: List[BikeTracker], icloud_key: bytes, within_last_hours: float = 24
) -> Dict[str, LocationReport]:
    AppleDSID, searchPartyToken = getAppleDSIDandSearchPartyToken(icloud_key)
    machineID, oneTimePassword = getOTPHeaders()
    UTCTime, Timezone, unixEpoch = getCurrentTimes()

    request_headers = {
        "Authorization": "Basic %s"
        % (
            base64.b64encode(
                (AppleDSID + ":" + searchPartyToken).encode("ascii")
            ).decode("ascii")
        ),
        "X-Apple-I-MD": "%s" % (oneTimePassword),
        "X-Apple-I-MD-RINFO": "17106176",
        "X-Apple-I-MD-M": "%s" % (machineID),
        "X-Apple-I-TimeZone": "%s" % (Timezone),
        "X-Apple-I-Client-Time": "%s" % (UTCTime),
        "Content-Type": "application/json",
        "Accept": "application/json",
        "X-BA-CLIENT-TIMESTAMP": "%s" % (unixEpoch),
    }

    startdate = int(unixEpoch - 60 * 60 * within_last_hours)

    data = json.dumps(
        {
            "search": [
                {
                    "endDate": (unixEpoch - 978307200) * 1000000,
                    "startDate": (startdate - 978307200) * 1000000,
                    "ids": [tracker.key_id for tracker in trackers],
                }
            ]
        }
    )

    response = requests.post(
        "https://gateway.icloud.com/acsnservice/fetch",
        headers=request_headers,
        data=data,
        timeout=10,
    )
    res = response.json()["results"]

    results = {}
    for report in res:
        tracker = [t for t in trackers if t.key_id == report["id"]][0]
        if tracker.name not in results:
            results[tracker.name] = []
        try:
            priv = bytes_to_int(base64.b64decode(tracker.private_key))
            data = base64.b64decode(report["payload"])

            # the following is adapted from https://github.com/hatomist/openhaystack-python, thanks @hatomist!
            timestamp = bytes_to_int(data[0:4]) + 978307200
            if timestamp >= startdate:
                eph_key_bytes = data[-CRYPTO_TAG_LENGTH - ENC_DATA_LENGTH - 57:-CRYPTO_TAG_LENGTH - ENC_DATA_LENGTH]
                eph_key = ec.EllipticCurvePublicKey.from_encoded_point(
                    ec.SECP224R1(), eph_key_bytes
                )
                shared_key = ec.derive_private_key(
                    priv, ec.SECP224R1(), default_backend()
                ).exchange(ec.ECDH(), eph_key)
                symmetric_key = sha256(shared_key + b"\x00\x00\x00\x01" + eph_key_bytes)
                decryption_key = symmetric_key[:16]
                iv = symmetric_key[16:]
                enc_data = data[-CRYPTO_TAG_LENGTH - ENC_DATA_LENGTH:-CRYPTO_TAG_LENGTH]
                tag = data[-CRYPTO_TAG_LENGTH:]

                decrypted = decrypt(
                    enc_data, algorithms.AES(decryption_key), modes.GCM(iv, tag)
                )
                result = decode_tag(decrypted, datetime.fromtimestamp(timestamp))
                results[tracker.name].append(result)
        except Exception as e:
            print(f"Got {type(e)} \"{e}\" while parsing report {report} for tracker {tracker.name}")
            continue
    return results
