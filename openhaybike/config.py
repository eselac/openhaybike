"""Gets info from the config of openhaybike."""

from pathlib import Path
import toml
from dataclasses import dataclass

@dataclass
class Config:
    cykel_endpoint: str
    cykel_api_key: str
    icloud_key: str

# TODO Make configurable
CONFIG_PATH = Path("config.toml")
CONFIG = Config(**toml.load(CONFIG_PATH))


