
quality = 5; // smaller => higher resolution
$fs = 0.1 * quality;
$fa = 1 * quality;

module top() {
    
    
    difference() {
        cylinder(10.0, 31.0/2, 31.0/2); // outer
        translate([0.0, 0.0, 1.5])
        cylinder(7.0, 25.5/2, 25.5/2); // inner
    }
    
    difference() {
        union() {
            
            translate([-6.0, -35.0, 0.0]) 
                cube([12.0, 70.0, 4.0]);
            
            translate([-0.0, -10.0, 0.0]) 
                cylinder(4.0, 10.0, 10.0);
            
            translate([-0.0, 10.0, 0.0]) 
                cylinder(4.0, 10.0, 10.0);
                    
            translate([0.0, -35.0, 0.0])
                cylinder(4.0, 12.0/2, 12.0/2);
            translate([0.0, 35.0, 0.0])
                cylinder(4.0, 12.0/2, 12.0/2);
        };
        cylinder(6.0, 25.0/2, 25.0/2); // inner
        translate([-5.5/2, -35.0, -1.0])
           cube([5.5, 15.0, 6.0]);
        translate([-5.5/2, 70.0-15.0-35.0, -1.0])
           cube([5.5, 15.0, 6.0]);


    }
    


}
top();
